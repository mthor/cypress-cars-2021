# Cars.com Project

## Notes About the Framework
The directions were very open ended so I selected Cypress.io as the test framework for the tests.  I did so because it is a fun and user-friendly framework to use and debug with.  It's also free, open-sourced, uses JavaScript, supports TypeScript and has incredible [documentation](https://docs.cypress.io/api/api/table-of-contents.html) and support in Stack Overflow.  

Instead of a pure Page Object Model, I followed Cypress's own [Best Practices recommendation](https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/#page-objects-problems) of using JavaScript functions (not even Cypress Commands) to allow for code reuse, readability and efficiency.  

Functions are better than Cypress Commands because you can 1) read/jump-to the definition and understand what you are using, and 2) utilize files/folder structure to organize your helper functions (instead of jamming an entire library into one file). 

## Notes About the Tests
In general, `Cars.com` is throwing many failing api calls (404s, 503s, cancelled, aborted) and producing dozens of console errors during test runs.  This is all out of our control and does make for a less than ideal testing situation.  Also, the pages are trying to load so many images and ads that it takes the DOM **FOREVER** to finish loading and provide a stable state to test.  

Please note **#3** (Select `New` radio button from New/Used) is commented out in the test.  This is becuase clicking the `New` radio button causes the `filter-overlay` that is supposed to temporarily cover the left menu during the filter polling process never goes away inside the test runner.  This is likely because the `Cars.com` app is using the incorrect polling mechanism.  Since the app is not ours and I cannot find out if it is using `interval()` or some other sort of timeout I cannot force the app to stop this behavior.  The test code and assertions is all written, it's just commented out because the `filter-overlay` gets stuck.

Same issue with **#5** (Select `Touring 8-Passenger` from Trim).  `filter-overly` gets stuck on and breaks the test.  The test code and assertion code is all there and ready to use, it's just commented out.

You can find the snapshot taken at the end of the test in the `cypress/snapshots` directory.  I am not testing that the snapshot matches the previously taken snapshot because the page will never match (far too dynamic).

## Getting Started
cd into the project root

## Install Project
`npm install`

## Open Cypress Test Runner
`npm run cy:open`

## Run Tests Individually
From the Cypress GUI (opened with the above command) select one at a time and watch Cypress open a browser and perform the test:
- `search.spec.js`
