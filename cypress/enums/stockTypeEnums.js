export const MainSearchStockType = {
    "All": "New & Used Cars",
    "New": "New Cars",
    "Used": "Used Cars",
    "Certified": "Certified Cars"
}

export const SearchResultsFilterBarStockType = {
    "All": "",
    "New": "New",
    "Used": "Used",
    "Certified": "Certified Used"
}