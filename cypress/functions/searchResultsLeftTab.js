// Assertions and Actions for the Search Results Left Tab section
import * as searchResultsFilter from "./searchResultsFilter"
import * as StockTypes from "../enums/stockTypeEnums"

export function stockTypeRadio(selection) {
    switch(selection) {
        case 'All':
            cy.get('input[id="stkTypId-any"]').click()
            waitUntilFilterOverlayIsHidden()
            searchResultsFilter.assertNoStockTypeFilter()
            break
        case 'New':
            cy.get('#stkTypId .radio').contains('New').click()
            waitUntilFilterOverlayIsHidden()
            searchResultsFilter.assertStockType(StockTypes.SearchResultsFilterBarStockType.New)
            break
        case 'Used':
            cy.get('#stkTypId .radio').contains('Used').click()
            waitUntilFilterOverlayIsHidden()
            searchResultsFilter.assertStockType(StockTypes.SearchResultsFilterBarStockType.Used)
            break
        case 'Certified':
            cy.get('#stkTypId .radio').contains('Certified').click()
            waitUntilFilterOverlayIsHidden()
            searchResultsFilter.assertStockType(StockTypes.SearchResultsFilterBarStockType.Certified)
            break
    }
}

export function trimSelector(selection) {
    cy.get('#trId').within(() => {
        cy.get('li').contains(selection).click()
    })
    searchResultsFilter.assertTrim(selection)
}

export function waitUntilFilterOverlayIsHidden() {
    cy.wait(500)
    // cy.reload()
    cy.get('.filter-overlay', { timeout: 60000 }).should('not.be.visible')
}