// Assertions and Actions for the Search Results Filter Bar section

export function standardFilterAssertions(price = null, make = null, model = null, stockType = null) {

    if (price) { assertMaxPrice(price) }
    if (make) { assertMake(make) }
    if (model) { assertModel(model) }
    if (stockType) { assertStockType(stockType) }
}

export function assertMaxPrice(price, exists = true) {
    if (exists) {
        cy.get('li[data-dimension-id="prMx"]').contains(price)
    } else {
        cy.get('li[data-dimension-id="prMx"]').contains(price).should('not.exist')
    }
}

export function assertMake(make, exists = true) {
    if (exists) {
        cy.get('li[data-dimension-id="mkId"]').contains(make)
    } else {
        cy.get('li[data-dimension-id="mkId"]').contains(make).should('not.exist')
    }
}

export function assertModel(model, exists = true) {
    if (exists) {
        cy.get('li[data-dimension-id="mdId"]').contains(model)
    } else {
        cy.get('li[data-dimension-id="mdId"]').contains(model).should('not.exist')
    }
}

export function assertStockType(stockType, exists = true) {
    if (exists) {
        cy.get('li[data-dimension-id="stkTypId"]').contains(stockType)
    } else {
        cy.get('li[data-dimension-id="stkTypId"]').contains(stockType).should('not.exist')
    }
}

export function assertTrim(trim, exists = true) {
    if (exists) {
        cy.get('li[data-dimension-id="trId"]').contains(trim)
    } else {
        cy.get('li[data-dimension-id="trId"]').contains(trim).should('not.exist')
    }
}

export function assertNoStockTypeFilter() {
    cy.get('li[data-dimension-id="stkTypId"]').should('not.exist')
}