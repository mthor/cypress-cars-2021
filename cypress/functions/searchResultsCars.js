// Actions for the actual search results

export function selectCarByNumber(number) {
    // Provide a number 1-n, this will grab that element based on array math
    cy.get('.shop-srp-listings__listing-container').eq(number - 1).click()
    
    cy.get('#vdpe-leadform', { timeout: 30000 }).should('be.visible')
}