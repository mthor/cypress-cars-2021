// Actions to take from the Main Search section on the home page

export function waitForMainSearch() {
    cy.get('a[data-linkname="Advanced Search"]', { timeout: 20000 }).should('be.visible')
}

export function fullSearch(stock, make, model, price, radius, zip) {
    stockSelect(stock)
    makeSelect(make)
    modelSelect(model)
    priceSelect(price)
    radiusSelect(radius)
    enterZip(zip)
    clickSearch()
}

export function stockSelect(option) {
    cy.get('select[name="stockType"]').select(option)
}

export function makeSelect(option) {
    cy.get('select[name="makeId"]').select(option)
}

export function modelSelect(option) {
    cy.get('select[name="modelId"]').select(option)
}

export function priceSelect(option) {
    cy.get('select[name="priceMax"]').select(option)
}

export function radiusSelect(option) {
    cy.get('select[name="radius"]').select(option)
}

export function enterZip(zip) {
    cy.get('input[name="zip"]').clear().type(zip)
}

// This action results in a new page, so do an intelligent wait for part of that page to be displayed
export function clickSearch() {
    cy.get('input[value="Search"]').click()
    cy.get('.matchcount', { timeout: 60000 }).should('be.visible')
}