// Actions for a single vehicle page

export function vehicleTitleContains(term) {
    cy.get('.vehicle-info__title-container').should(($el) => {
        expect($el).to.contain(term)
    })
}

export function checkAvailabilityDisplayed() {
    cy.get('button[data-linkname="submit-email-lead"]').should('be.visible')
}

export function fillOutContactSeller(firstName = null, lastName = null, email = null) {
    if (firstName) {
        cy.get('input[name="fname"]').clear().type(firstName)
    }
    if (lastName) {
        cy.get('input[name="lname"]').clear().type(lastName)
    }
    if (email) {
        cy.get('input[name="email"]').clear().type(email)
    }
}

export function scrollToPaymentCalculator() {
    cy.get('.vdp-pricing__container').scrollIntoView({ offset: {top: -100, left: 0} })
}